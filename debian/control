Source: cub
Section: libdevel
Priority: optional
Maintainer: Martin Tammvee <martin.tammvee@clevon.com>
Homepage: https://github.com/NVIDIA/cub
Vcs-Browser: https://gitlab.com/clevon/debian/cub
Vcs-Git: https://gitlab.com/clevon/debian/cub.git
Rules-Requires-Root: no
Standards-Version: 4.6.0
Build-Depends: cmake, debhelper-compat (= 13), ninja-build

Package: libcub-dev
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: Cooperative primitives for CUDA C++ (development files)
 CUB provides state-of-the-art, reusable software components for every layer
 of the CUDA programming language.
  * Parallel primitives
   * Warp-wide "collective" primitives
   * Block-wide "collective" primitives
   * Device-wide primitives
  * Utilities
   * Fancy iterators
   * Thread and thread block I/O
   * PTX intrinsics
   * Device, kernel, and storage management

Package: cub-samples
Section: devel
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: Cooperative primitives for CUDA C++ (samples)
 CUB provides state-of-the-art, reusable software components for every layer
 of the CUDA programming language.
